//
//  TapColorViewController.swift
//  AutoLayout
//
//  Created by Wilson Gabriel Ramos Bravo on 4/7/18.
//  Copyright © 2018 Wilson Gabriel Ramos Bravo. All rights reserved.
//

import UIKit

class TapColorViewController: UIViewController {

    @IBOutlet weak var greenView: UIView!
    @IBOutlet weak var blueView: UIView!
    @IBOutlet weak var redView: UIView!
    @IBOutlet weak var yellowView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func onetapActivated(_ sender: Any) {
        greenView.backgroundColor = .yellow
    }
    
    @IBAction func twoTapActivated(_ sender: Any) {
        blueView.backgroundColor = .black
    }
    
    
    @IBAction func threeTapActivated(_ sender: Any) {
        redView.backgroundColor = .blue
    }
    
    @IBAction func fourTapActivated(_ sender: Any) {
        yellowView.backgroundColor = .red
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
