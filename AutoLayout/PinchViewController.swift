//
//  PinchViewController.swift
//  AutoLayout
//
//  Created by Wilson Gabriel Ramos Bravo on 27/6/18.
//  Copyright © 2018 Wilson Gabriel Ramos Bravo. All rights reserved.
//

import UIKit

class PinchViewController: UIViewController {

    @IBOutlet weak var heightC: NSLayoutConstraint!
    @IBOutlet weak var widthC: NSLayoutConstraint!
    @IBOutlet weak var squareView: UIView!
    @IBOutlet weak var positionX: NSLayoutConstraint!
    @IBOutlet weak var positionY: NSLayoutConstraint!
    
    
    var originalHeight: CGFloat = 0
    var originalWidth: CGFloat = 0
    var originalRotationX: CGFloat = 0
    var originalRotationY: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        originalHeight = heightC.constant
        originalWidth = widthC.constant
        originalRotationX = positionX.constant
        originalRotationY = positionY.constant
        // Do any additional setup after loading the view.
    }

   @IBAction func pincGestureActivated(_ sender: UIPinchGestureRecognizer) {
        heightC.constant = originalHeight * sender.scale
        widthC.constant = originalWidth * sender.scale
        if(sender.state == .ended) {
     
            UIView.animate(withDuration: 3){
                self.heightC.constant = self.originalHeight
                self.widthC.constant = self.originalWidth
            }
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func longGestureActivated(_ sender: Any) {
        squareView.backgroundColor = .red
    }
    
    @IBAction func rotationGestureActivated(_ sender: UIRotationGestureRecognizer) {
        guard sender.view != nil else {return
        }
        if sender.state == .began || sender.state == .changed {
            sender.view?.transform = sender.view!.transform.rotated(by: sender.rotation)
            sender.rotation = 0
        }
        if sender.state == .ended {
            UIView.animate(withDuration: 0.5,delay: 0.1, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.0, options: .curveEaseOut, animations: {sender.view?.transform = CGAffineTransform(rotationAngle: CGFloat(0.0))})
        }
        
    }
    
    
}
