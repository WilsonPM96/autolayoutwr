//
//  TapViewController.swift
//  AutoLayout
//
//  Created by Wilson Gabriel Ramos Bravo on 27/6/18.
//  Copyright © 2018 Wilson Gabriel Ramos Bravo. All rights reserved.
//

import UIKit

class TapViewController: UIViewController {

    @IBOutlet weak var mainView: UIView!
    
    @IBAction func tapGestureActivated(_ sender: Any) {
        mainView.backgroundColor = .red
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  

}
